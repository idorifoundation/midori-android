package org.midorinext.android.browser

import org.midorinext.android.preference.IntEnum

enum class SuggestionNumChoice(override val value: Int) : IntEnum{
    THREE(0),
    FOUR(1),
    FIVE(2),
    SIX(3),
    SEVEN(4),
    EIGHT(5)
}